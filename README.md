# Aplikasi Pembayaran #

Menjalankan aplikasi :

1. Buat user database

    ```
    createuser -P namauser
    ```

2. Buat database

    ```
    createdb -Onamauser namadatabase
    ```

3. Jalankan aplikasi

   ```
   mvn clean spring-boot:run
   ```
   
## Deployment ke Heroku ##

1. Buat akun dan login ke [heroku.com](https://heroku.com)

2. Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

3. Login dengan Heroku CLI di command line

    ```
    heroku login 
    heroku: Press any key to open up the browser to login or q to exit: 
    Opening browser to https://cli-auth.heroku.com/auth/cli/browser/495c08df-4ac5-42ff-86a1-b251be618793
    Logging in... done
    Logged in as endy.muhardin@gmail.com
    ```

4. Buat project baru di Heroku, misalnya kita beri nama `tms202001-pembayaran`

    ```
    heroku apps:create -a tms202001-pembayaran
    ```

5. Push source code ke Heroku untuk mendeploy

    ```
    git push heroku master
    ```

6. Tampilkan log Heroku untuk memudahkan debug

    ```
    heroku logs --tail
    ```

7. Browse ke Heroku kalau aplikasi berhasil terdeploy dengan sukses. Alamatnya biasanya berbentuk `https://<nama-project>.herokuapp.com`

## Konversi Menjadi Config Client ##

1. Tambahkan dependensi

2. Ubah file `application.properties` menjadi `bootstrap.properties`

3. Sediakan konfigurasi di config server