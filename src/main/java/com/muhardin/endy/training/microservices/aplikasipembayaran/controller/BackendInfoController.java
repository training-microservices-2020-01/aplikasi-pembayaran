package com.muhardin.endy.training.microservices.aplikasipembayaran.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController @Slf4j
public class BackendInfoController {

    @GetMapping("/api/backendinfo")
    public Map<String, Object> backendInfo(HttpServletRequest request) {
        String alamatIp = request.getLocalAddr();
        Integer port = request.getLocalPort();

        Map<String, Object> hasil = new LinkedHashMap<>();
        hasil.put("ipAddress", alamatIp);
        hasil.put("port",port);
        hasil.put("hostname", request.getLocalName());

        log.info("Backend info : {}", hasil);

        return hasil;
    }
}
