package com.muhardin.endy.training.microservices.aplikasipembayaran.controller;

import com.muhardin.endy.training.microservices.aplikasipembayaran.dao.CustomerDao;
import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @Slf4j
public class CustomerController {

    @Autowired private CustomerDao customerDao;

    @GetMapping("/api/customer/")
    public Iterable<Customer> daftarCustomer() {
        log.info("Query data customer");
        return customerDao.findAll();
    }
}
