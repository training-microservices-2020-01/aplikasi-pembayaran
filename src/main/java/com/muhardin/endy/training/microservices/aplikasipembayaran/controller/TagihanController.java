package com.muhardin.endy.training.microservices.aplikasipembayaran.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muhardin.endy.training.microservices.aplikasipembayaran.dao.TagihanDao;
import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.Tagihan;
import com.muhardin.endy.training.microservices.aplikasipembayaran.service.KafkaSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class TagihanController {

    @Autowired private TagihanDao tagihanDao;
    @Autowired private KafkaSenderService kafkaSenderService;;

    @PostMapping("/api/tagihan/")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTagihan(@RequestBody @Valid Tagihan tagihan) throws JsonProcessingException {
        tagihanDao.save(tagihan);
        kafkaSenderService.kirim(tagihan);
    }


}
