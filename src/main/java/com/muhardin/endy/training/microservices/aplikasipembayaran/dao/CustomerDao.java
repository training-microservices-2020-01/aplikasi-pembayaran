package com.muhardin.endy.training.microservices.aplikasipembayaran.dao;

import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
