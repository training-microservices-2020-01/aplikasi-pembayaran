package com.muhardin.endy.training.microservices.aplikasipembayaran.dao;

import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.Tagihan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TagihanDao extends PagingAndSortingRepository<Tagihan, String> {
    Tagihan findByNomor(String nomorTagihan);
}
