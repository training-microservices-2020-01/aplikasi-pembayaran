package com.muhardin.endy.training.microservices.aplikasipembayaran.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TagihanResponse {
    private String nomorTagihan;
    private String nama;
    private BigDecimal nilaiTagihan;
    private String bank;
    private String nomorVa;
}
