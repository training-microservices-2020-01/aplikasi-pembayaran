package com.muhardin.endy.training.microservices.aplikasipembayaran.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.microservices.aplikasipembayaran.dao.TagihanDao;
import com.muhardin.endy.training.microservices.aplikasipembayaran.dao.VirtualAccountDao;
import com.muhardin.endy.training.microservices.aplikasipembayaran.dto.TagihanResponse;
import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.Tagihan;
import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.VirtualAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service @Slf4j
public class KafkaListenerService {

    @Autowired private TagihanDao tagihanDao;
    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(
            autoStartup = "${kafka.enabled:false}",
            topics = "${kafka.topic.tagihan.response:tagihan-response-dev}"
    )
    public void terimaTagihanResponse(String msg) throws JsonProcessingException {
        log.info("Terima response : {}", msg);
        TagihanResponse response = objectMapper.readValue(msg, TagihanResponse.class);

        Tagihan t = tagihanDao.findByNomor(response.getNomorTagihan());

        VirtualAccount va = new VirtualAccount();
        va.setTagihan(t);
        va.setBank(response.getBank());
        va.setNomorVa(response.getNomorVa());
        virtualAccountDao.save(va);
    }
}
